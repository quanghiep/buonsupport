package com.example.buonsupport.constant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constant {
    public static final List<String> DISTRICTS = Arrays.asList("Huyện Đơn Dương");
    public static final Map<String, List<String>> VILLAGES;
    static {
        VILLAGES = new HashMap<String, List<String>>() {
            {
                put(DISTRICTS.get(0), Arrays.asList("Thị trấn DRan", "Thị trấn Thạnh Mỹ", "Xã Lạc Xuân", "Xã Đạ Ròn", "Xã Lạc Lâm"
                    , "Xã Ka Đô", "Xã Quảng Lập", "Xã Ka Đơn", "Xã Tu Tra", "Xã Pró"));
            }
        };
    }
    public static final List<String> AGRICULTURES = Arrays.asList("Cà chua", "Cà tím", "Xà lách", "Cải thảo", "Ớt tây");
}
