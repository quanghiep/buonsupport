package com.example.buonsupport.service;

import com.example.buonsupport.modal.StatisticalMoney;
import com.example.buonsupport.repository.HarvestRepository;
import com.example.buonsupport.repository.PurchaseFeedRepository;
import com.example.buonsupport.repository.StatisticalMoneyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;

@Service
public class StatisticalService {

    @Autowired
    private PurchaseFeedRepository purchaseFeedRepository;

    @Autowired
    private HarvestRepository harvestRepository;

    @Autowired
    private StatisticalMoneyRepository statisticalMoneyRepository;

    @Scheduled(cron = "0 1 0 * * *")
    private void statisticalMoney() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date prevDate = new Date(cal.getTime().getTime());

        Long moneyOut = purchaseFeedRepository.sumPriceByDate(prevDate);
        Long moneyIn = harvestRepository.sumPriceByDate(prevDate);

        StatisticalMoney money = StatisticalMoney.builder().date(cal.getTime()).moneyIn(moneyIn).moneyOut(moneyOut).build();
        statisticalMoneyRepository.save(money);
    }
}
