package com.example.buonsupport.service;

import com.example.buonsupport.modal.Agriculture;
import com.example.buonsupport.modal.Village;
import com.example.buonsupport.repository.AgricultureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AgricultureService {

    @Autowired
    private AgricultureRepository agricultureRepository;

    public ResponseEntity<?> saveAgriculture(Agriculture agriculture) {
        agricultureRepository.save(agriculture);
        return ResponseEntity.ok(agriculture.getName());
    }
}
