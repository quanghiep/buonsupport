package com.example.buonsupport.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class LoggerService {

    public static final Logger LOGGER = LogManager.getLogger(LoggerService.class);
    private Logger logger;

    public LoggerService(java.lang.Class<?> clazz) {
        this.logger = LogManager.getLogger(clazz);
    }

    public void info(String message) {
        logger.info("[" + getAuthorName() + "] " + message);
    }

    public String getAuthorName() {
        Authentication author = SecurityContextHolder.getContext().getAuthentication();
        return author != null ? author.getName() : "anonymous";
    }

    public static void INFO(String message) {
        String author = SecurityContextHolder.getContext().getAuthentication().getName();
        LOGGER.info("[" + author + "] " + message);
    }
}
