package com.example.buonsupport.service;

import com.example.buonsupport.modal.Agriculture;
import com.example.buonsupport.modal.Farmer;
import com.example.buonsupport.modal.Purchase;
import com.example.buonsupport.modal.Village;
import com.example.buonsupport.repository.AgricultureRepository;
import com.example.buonsupport.repository.FarmerRepository;
import com.example.buonsupport.repository.PurchaseRepository;
import com.example.buonsupport.repository.VillageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    private FarmerRepository farmerRepository;

    @Autowired
    private AgricultureRepository agricultureRepository;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private VillageRepository villageRepository;

    public ResponseEntity<?> savePurchase(Purchase purchase) {
        Farmer farmer = farmerRepository.findByPhone(purchase.getFarmer().getPhone());
        if(farmer != null)
            purchase.setFarmer(farmer);
        else {
            farmer = purchase.getFarmer();
            farmerRepository.save(farmer);
            purchase.setFarmer(farmer);
        }

        Optional<Agriculture> agriculture = agricultureRepository.findById(purchase.getAgriculture().getId());
        Optional<Village> village = villageRepository.findByName(purchase.getVillage().getName());

        if(agriculture.isPresent() && village.isPresent()) {
            purchase.setAgriculture(agriculture.get());
            purchase.setVillage(village.get());
            purchaseRepository.save(purchase);
            return ResponseEntity.ok(purchase);
        } else {
            return ResponseEntity.badRequest().body("agriculture or village is not present");
        }
    }

    public ResponseEntity<?> updatePurchase(Purchase purchase) {
        Optional<Purchase> dbPurchase = purchaseRepository.findById(purchase.getId());
        if(dbPurchase.isPresent()) {

            return savePurchase(purchase);
        } else {
            return ResponseEntity.badRequest().body("Purchase is not found");
        }
    }
}
