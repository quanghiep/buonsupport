package com.example.buonsupport.service;

import com.example.buonsupport.modal.District;
import com.example.buonsupport.modal.Village;
import com.example.buonsupport.repository.DistrictRepository;
import com.example.buonsupport.repository.VillageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    VillageRepository villageRepository;

    public ResponseEntity<?> saveDistrict(District district) {
        districtRepository.save(district);
        return ResponseEntity.ok(district.getName());
    }

    public ResponseEntity<?> saveVillage(Village village) {
        villageRepository.save(village);
        return ResponseEntity.ok(village.getName());
    }
}
