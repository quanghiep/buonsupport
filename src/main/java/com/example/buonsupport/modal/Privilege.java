package com.example.buonsupport.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Table(name = "privilege")
@NoArgsConstructor
@Entity
public class Privilege extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "display_name")
    private String displayName;

    @JsonIgnore
    @ManyToMany(mappedBy = "privileges")
    List<Role> roles = new ArrayList<>();

    public Privilege(String name, String displayName) {
        this.displayName = displayName;
        this.name = name;
    }
}
