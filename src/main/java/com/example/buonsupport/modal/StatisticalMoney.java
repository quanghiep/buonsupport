package com.example.buonsupport.modal;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "statistical_money")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StatisticalMoney {
    @Id
    @Column(name = "date")
    private Date date;

    @Column(name = "money_in")
    private Long moneyIn;

    @Column(name = "money_out")
    private Long moneyOut;
}
