package com.example.buonsupport.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "purchase")
@Getter
@Setter
public class Purchase extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "acreage")
    private Long acreage;

    @Column(name= "amount")
    private Long amount;

    @Column(name = "price")
    private Long price;

    @Formula("(select coalesce(sum(f.price),0) from purchase_feed f where f.purchase_id = id)")
    private Long moneyOut;

    @Formula("(select coalesce(sum(h.price),0) from harvest h where h.purchase_id = id)")
    private Long moneyIn;

    @Column(name = "note")
    private String note;

    @Column(name="lat")
    private double lat;

    @Column(name="lng")
    private double lng;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private purchaseStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "farmer_id")
    private Farmer farmer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "village_id")
    private Village village;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agriculture_id")
    private Agriculture agriculture;

    @JsonIgnore
    @OneToMany(mappedBy = "purchase")
    private List<PurchaseFeed> purchaseFeeds = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "purchase")
    private List<Harvest> harvests = new ArrayList<>();

    public static enum purchaseStatus {
        BEFORE_HARVEST,
        IN_HARVEST,
        AFTER_HARVEST
    }
}
