package com.example.buonsupport.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "purchase_feed")
@Getter
@Setter
public class PurchaseFeed extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "kind")
    private String kind;

    @Column(name = "price")
    private Long price;

    @Column(name = "note")
    private String note;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_id")
    private Purchase purchase;

    public void updateFeed(PurchaseFeed feed) {
        this.kind = feed.getKind();
        this.price = feed.getPrice();
        this.note = feed.getNote();
    }
}
