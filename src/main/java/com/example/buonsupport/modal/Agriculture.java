package com.example.buonsupport.modal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "agriculture")
@Getter
@Setter
@NoArgsConstructor
public class Agriculture extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "unit")
    private String unit;

    public Agriculture(String name) {
        this.name = name;
    }
}
