package com.example.buonsupport.controller;

import com.example.buonsupport.modal.Purchase;
import com.example.buonsupport.repository.PurchaseRepository;
import com.example.buonsupport.service.LoggerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private PurchaseRepository purchaseRepository;

    private final LoggerService logger = new LoggerService(HomeController.class);

    @RequestMapping("/admin")
    public String admin(Model model) {
        model.addAttribute(Purchase.purchaseStatus.BEFORE_HARVEST.toString(), 0);
        model.addAttribute(Purchase.purchaseStatus.IN_HARVEST.toString(), 0);
        model.addAttribute(Purchase.purchaseStatus.AFTER_HARVEST.toString(), 0);
        purchaseRepository.groupByStatus().forEach(objects -> {
            if(objects[0] != null && objects[1] != null)
                model.addAttribute(objects[0].toString(), objects[1].toString());
        });
        return "/home";
    }

    @RequestMapping(value = "/admin/agriculture/statistical")
    public ResponseEntity<?> agricultureStatistical(@RequestParam("from") String from, @RequestParam("to") String to) {
        Date dFrom = Date.valueOf(from);
        Date dTo = Date.valueOf(to);

        return ResponseEntity.ok(purchaseRepository.groupByAgriculture(dFrom, dTo));
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "/login";
    }

    @RequestMapping("/403")
    public String error403(Model model) {
        return "/login";
    }
}
