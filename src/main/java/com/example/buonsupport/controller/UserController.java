package com.example.buonsupport.controller;

import com.example.buonsupport.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public String listUser(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "user-list";
    }

    @RequestMapping(value = "/authentication", method = RequestMethod.GET)
    public String userAuthentication(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "user-authentication";
    }
}
