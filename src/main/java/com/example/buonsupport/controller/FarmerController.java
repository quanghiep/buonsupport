package com.example.buonsupport.controller;

import com.example.buonsupport.repository.FarmerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/customer")
public class FarmerController {

    @Autowired
    private FarmerRepository farmerRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listCustomer(Model model) {
        model.addAttribute("customers", farmerRepository.findAll());
        return "customer-list";
    }
}
