package com.example.buonsupport.controller;

import com.example.buonsupport.modal.*;
import com.example.buonsupport.repository.*;
import com.example.buonsupport.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class PurchaseController {

    @Autowired
    private AgricultureRepository agricultureRepository;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private PurchaseFeedRepository purchaseFeedRepository;

    @Autowired
    private HarvestRepository harvestRepository;

    @RequestMapping(value = "/purchase", method = RequestMethod.GET)
    public String purchase(Model model) {
        model.addAttribute("agriculture", agricultureRepository.findAll());
        Purchase purchase = new Purchase();
        model.addAttribute("purchase", purchase);
        return "purchase";
    }

    @RequestMapping(value = "/purchase/{purchaseId}", method = RequestMethod.GET)
    public String purchase(Model model, @PathVariable(value = "purchaseId") Long purchaseId) {
        model.addAttribute("agriculture", agricultureRepository.findAll());
        Purchase purchase = new Purchase();
        if(purchaseId != null) {
            purchase = purchaseRepository.findById(purchaseId).get();
        }
        model.addAttribute("purchase", purchase);
        return "purchase";
    }

    @RequestMapping(value = "/purchase/list", method = RequestMethod.GET)
    public String listPurchase(Model model) {
        model.addAttribute("purchases", purchaseRepository.findAll());
        return "purchase-list";
    }

    @RequestMapping(value = "/agriculture/create")
    public ResponseEntity<?> createAgriculture(@RequestAttribute("agriculture") Agriculture agriculture) {
        agricultureRepository.save(agriculture);
        return ResponseEntity.ok(agriculture);
    }

    @RequestMapping(value = "/purchase/create", method = RequestMethod.POST)
    public String createPurchase(@ModelAttribute("purchase") Purchase purchase) {
        purchase.setId(null);
        purchase.setStatus(Purchase.purchaseStatus.BEFORE_HARVEST);
        purchaseService.savePurchase(purchase);

        return "redirect:/admin/purchase/list";
    }

    @RequestMapping(value = "/purchase/update", method = RequestMethod.POST)
    public String updatePurchase(@ModelAttribute("purchase") Purchase purchase) {
        purchaseService.updatePurchase(purchase);

        return "redirect:/admin/purchase/list";
    }

    @RequestMapping(value = "/purchase/{purchaseId}/feed", method = RequestMethod.GET)
    public String purchaseFeed(Model model, @PathVariable(value = "purchaseId") Long purchaseId) {
        Optional<Purchase> purchase = purchaseRepository.findById(purchaseId);
        if(!purchase.isPresent()) {
            model.addAttribute("message", "purchase not found");
            return "error/bad-request";
        }
        model.addAttribute("feeds", purchase.get().getPurchaseFeeds());
        model.addAttribute("purchaseId", purchaseId);
        return "purchase-feed";
    }

    @RequestMapping(value = "/purchase/{purchaseId}/feed/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> purchaseFeedAdd(@PathVariable(value = "purchaseId") Long purchaseId, @ModelAttribute("purchaseFeed") PurchaseFeed purchaseFeed) {
        Optional<Purchase> purchase = purchaseRepository.findById(purchaseId);
        if(!purchase.isPresent()) {
            return ResponseEntity.badRequest().body("purchase not found");
        }

        purchaseFeed.setId(null);
        purchaseFeed.setPurchase(purchase.get());
        purchaseFeedRepository.save(purchaseFeed);
        purchaseRepository.save(purchase.get());

        return ResponseEntity.ok(purchaseFeed);
    }

    @RequestMapping(value = "/purchase/feed/update", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> purchaseFeedUpdate(@ModelAttribute("purchaseFeed") PurchaseFeed purchaseFeed) {
        Optional<PurchaseFeed> dbPurchaseFeed = purchaseFeedRepository.findById(purchaseFeed.getId());
        if(!dbPurchaseFeed.isPresent()) {
            return ResponseEntity.badRequest().body("Purchase Feed not found");
        }

        dbPurchaseFeed.get().updateFeed(purchaseFeed);
        purchaseFeedRepository.save(dbPurchaseFeed.get());

        return ResponseEntity.ok(dbPurchaseFeed.get());
    }

    @RequestMapping(value = "/purchase/feed/{feedId}/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> purchaseFeedDelete(@PathVariable(value = "feedId") Long feedId) {
        purchaseFeedRepository.deleteById(feedId);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/purchase/{purchaseId}/harvest", method = RequestMethod.GET)
    public String purchaseHarvest(Model model, @PathVariable(value = "purchaseId") Long purchaseId) {
        Optional<Purchase> purchase = purchaseRepository.findById(purchaseId);
        if(!purchase.isPresent()) {
            model.addAttribute("message", "purchase not found");
            return "error/bad-request";
        }
        model.addAttribute("harvest", purchase.get().getHarvests());
        model.addAttribute("purchaseId", purchaseId);
        return "purchase-harvest";
    }

    @RequestMapping(value = "/purchase/{purchaseId}/harvest/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> purchaseHarvestAdd(@PathVariable(value = "purchaseId") Long purchaseId, @ModelAttribute("harvest") Harvest harvest) {
        Optional<Purchase> purchase = purchaseRepository.findById(purchaseId);
        if(!purchase.isPresent()) {
            return ResponseEntity.badRequest().body("purchase not found");
        }

        harvest.setId(null);
        harvest.setPurchase(purchase.get());
        purchase.get().setStatus(Purchase.purchaseStatus.IN_HARVEST);
        harvestRepository.save(harvest);

        return ResponseEntity.ok(harvest);
    }

    @RequestMapping(value = "/purchase/harvest/update", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> purchaseHarvestUpdate(@ModelAttribute("harvest") Harvest harvest) {
        Optional<Harvest> dbHarvest = harvestRepository.findById(harvest.getId());
        if(!dbHarvest.isPresent()) {
            return ResponseEntity.badRequest().body("Purchase Feed not found");
        }

        dbHarvest.get().updateHarvest(harvest);
        harvestRepository.save(dbHarvest.get());

        return ResponseEntity.ok(dbHarvest.get());
    }

    @RequestMapping(value = "/purchase/harvest/{harvestId}/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> purchaseHarvestDelete(@PathVariable(value = "harvestId") Long harvestId) {
        Optional<Harvest> harvest = harvestRepository.findById(harvestId);
        if(!harvest.isPresent())
            return ResponseEntity.badRequest().body("Harvest not found");

        int countHarvest = harvestRepository.countAllByPurchase_Id(harvest.get().getId());
        if(countHarvest == 0) {
            Purchase purchase = harvest.get().getPurchase();
            purchase.setStatus(Purchase.purchaseStatus.BEFORE_HARVEST);
            purchaseRepository.save(purchase);
        }

        harvestRepository.deleteById(harvestId);

        return ResponseEntity.ok().build();
    }
}