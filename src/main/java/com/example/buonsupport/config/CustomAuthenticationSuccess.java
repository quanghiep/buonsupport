package com.example.buonsupport.config;

import com.example.buonsupport.modal.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class CustomAuthenticationSuccess implements AuthenticationSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationSuccess.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {

        logger.info("/******************/START-LOGIN - " + authentication.getName() + "/*********************************/");

        try {
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(Role.ROLE.ADMIN.toString()))) {
                httpServletResponse.sendRedirect("/admin");
            } else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(Role.ROLE.USER.toString()))) {
                httpServletResponse.sendRedirect("/admin");
            } else {
                logger.error("CustomAuthenticationSuccess Fail. No authentication math");
                httpServletResponse.sendRedirect("/login?error=notAuthMath");
            }

        } catch (Exception e) {
            logger.error("CustomAuthenticationSuccess Fail. " + e.getMessage());
            httpServletResponse.sendRedirect("/login?error=" + e.getMessage());
        } finally {
            logger.info("/******************/END-LOGIN - " + authentication.getName() + "/*********************************/");
        }
    }
}
