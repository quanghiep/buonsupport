package com.example.buonsupport.config;

import com.example.buonsupport.Exception.MyAccessDeniedHandler;
import com.example.buonsupport.modal.Role;
import com.example.buonsupport.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@Configuration
@EnableWebSecurity
@Transactional
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private MyAccessDeniedHandler myAccessDeniedHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/img/*", "/css/*", "/js/*", "/login/**").permitAll()
                .antMatchers("/admin/**").hasAnyAuthority(Role.ROLE.ADMIN.toString())
                .antMatchers("/user/**").hasAnyAuthority(Role.ROLE.USER.toString())
                .anyRequest().authenticated().and()
                .formLogin()
                .loginProcessingUrl("/login/request")
                .loginPage("/login").usernameParameter("username").passwordParameter("password")
                .successHandler(new CustomAuthenticationSuccess())
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and().exceptionHandling().accessDeniedHandler(myAccessDeniedHandler);
    }
}
