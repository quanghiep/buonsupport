package com.example.buonsupport;

import com.example.buonsupport.constant.Constant;
import com.example.buonsupport.modal.*;
import com.example.buonsupport.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class InitData implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private VillageRepository villageRepository;

    @Autowired
    private AgricultureRepository agricultureRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    List<Privilege> privileges = Arrays.asList(
            new Privilege("ADD_PURCHASE", "TẠO TIN MUA"),
            new Privilege("ADD_PURCHASE_FEED", "THÊM CHI"),
            new Privilege("ADD_PURCHASE_HARVEST", "THÊM THU"),
            new Privilege("VIEW_CHART", "XEM THỐNG KÊ"),
            new Privilege("ADMIN", "QUẢN TRỊ"));

    public void run(ApplicationArguments args) {
        if(roleRepository.findAll().size() == 0) {
            roleRepository.save(new Role(Role.ROLE.ADMIN.toString()));
            roleRepository.save(new Role(Role.ROLE.USER.toString()));

            Role role = roleRepository.findByRoleName(Role.ROLE.ADMIN.toString());
            if (role != null) {
                List<Role> roles = new ArrayList<>();
                roles.add(role);
                userRepository.save(new User("admin",new BCryptPasswordEncoder().encode("admin"), roles));
            }
        }

        if(districtRepository.findAll().size() == 0) {
            List<District> districts = Constant.DISTRICTS.stream().map(District::new).collect(Collectors.toList());
            districtRepository.saveAll(districts);
        }

        if(villageRepository.countAllBy() == 0) {
            Constant.VILLAGES.forEach((k,v) -> {
                Optional<District> district = districtRepository.findByName(k);
                if(district.isPresent()) {
                    List<Village> villages = v.stream().map(village -> {
                        return new Village(village, district.get());
                    }).collect(Collectors.toList());
                    villageRepository.saveAll(villages);
                }
            });
        }

        if(agricultureRepository.countAllBy() == 0) {
            List<Agriculture> agricultures = Constant.AGRICULTURES.stream().map(Agriculture::new).collect(Collectors.toList());
            agricultureRepository.saveAll(agricultures);
        }

        List<Privilege> dbPrivilege = privilegeRepository.findAll();
        if(dbPrivilege.size() < privileges.size()) {
            Role role = roleRepository.findByRoleName(Role.ROLE.ADMIN.toString());
            List<Privilege> newPrivilege = new ArrayList<>();
            for (Privilege privilege : privileges)
                if (!dbPrivilege.contains(privilege)) {
                    privilege.setRoles(Collections.singletonList(role));
                    newPrivilege.add(privilege);
                }
            privilegeRepository.saveAll(newPrivilege);
        }
    }
}
