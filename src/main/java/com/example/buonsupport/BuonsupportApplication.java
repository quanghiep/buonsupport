package com.example.buonsupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BuonsupportApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuonsupportApplication.class, args);
    }

}
