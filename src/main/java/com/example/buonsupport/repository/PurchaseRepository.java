package com.example.buonsupport.repository;

import com.example.buonsupport.modal.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

    @Query("select p.status, count(p.id) from Purchase p group by p.status")
    List<Object[]> groupByStatus();

    @Query("select p.agriculture.name, count(p.id) from Purchase p where p.createdAt >= ?1 and p.createdAt <= ?2 group by p.agriculture")
    List<Object[]> groupByAgriculture(Date from, Date to);
}
