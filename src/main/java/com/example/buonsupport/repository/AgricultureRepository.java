package com.example.buonsupport.repository;

import com.example.buonsupport.modal.Agriculture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgricultureRepository extends JpaRepository<Agriculture, Long> {

    Optional<Agriculture> findByName(String name);

    int countAllBy();
}
