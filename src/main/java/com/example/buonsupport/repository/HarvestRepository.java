package com.example.buonsupport.repository;

import com.example.buonsupport.modal.Harvest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;

@Repository
public interface HarvestRepository extends JpaRepository<Harvest, Long> {

    int countAllByPurchase_Id(Long purchaseId);

    @Query("select sum(h.price) from Harvest h where h.createdAt = ?1")
    Long sumPriceByDate(Date date);
}
