package com.example.buonsupport.repository;

import com.example.buonsupport.modal.Village;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VillageRepository extends JpaRepository<Village, Long> {

    int countAllBy();

    Optional<Village> findByName(String name);
}
