package com.example.buonsupport.repository;

import com.example.buonsupport.modal.PurchaseFeed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;

@Repository
public interface PurchaseFeedRepository extends JpaRepository<PurchaseFeed, Long> {

    @Query("select sum(f.price) from PurchaseFeed f where f.createdAt = ?1")
    Long sumPriceByDate(Date date);
}
