package com.example.buonsupport.repository;

import com.example.buonsupport.modal.StatisticalMoney;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface StatisticalMoneyRepository extends JpaRepository<StatisticalMoney, Date> {
}
