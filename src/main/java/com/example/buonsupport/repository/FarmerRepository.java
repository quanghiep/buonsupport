package com.example.buonsupport.repository;

import com.example.buonsupport.modal.Farmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FarmerRepository extends JpaRepository<Farmer, Long> {

    Farmer findByPhone(String phone);
}
