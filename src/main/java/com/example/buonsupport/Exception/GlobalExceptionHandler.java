package com.example.buonsupport.Exception;

import com.example.buonsupport.service.LoggerService;
import org.apache.logging.log4j.LogManager;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {
    private LoggerService logger = new LoggerService(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ModelAndView handleInternalException(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation
                (e.getClass(), ResponseStatus.class) != null)
            throw e;

        logger.info("GlobalExceptionHandler: " + e.getMessage());
        String stackTrace = Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\r\n "));
        logger.info("GlobalStackTrace: " + stackTrace);
        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", request.getRequestURL());

        if(response.getStatus() == HttpStatus.NOT_FOUND.value()) {
            mav.setViewName("error/not-found");
        } else if (response.getStatus() == HttpStatus.BAD_REQUEST.value()) {
            mav.setViewName("error/bad-request");
        } else {
            mav.setViewName("error/internal");
        }

        return mav;
    }
}
