
var initAddress = "Tỉnh Lâm Đồng";
var districtAddress = "Huyện Đơn Dương, Tỉnh Lâm Đồng";
var map;
var geocoder = new google.maps.Geocoder();
var myMarker = null;
var mapZoom = 8;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom : mapZoom,
        mapTypeId : 'roadmap',
        mapTypeControl: false,
        streetViewControl: false
    });

    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    $('#pac-input').val(districtAddress);

    if(dbVillage == null)
        loadGeocoder(districtAddress, 12);
    else
        loadGeocoder(dbVillage + ", " + districtAddress, 14)

    map.addListener('click', function(e) {
        placeMarkerAndPanTo(e.latLng, map);
        $("#map_lat").val(e.latLng.lat());
        $("#map_lng").val(e.latLng.lng());
    });

    if(dbLng != null && dbLat != null) {
        placeMarkerAndPanTo({lat: dbLat, lng: dbLng}, map);
    }

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        var markers = [];
        // Clear out the old markers.
        markers.forEach(function(marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            var icon = {
                url : place.icon,
                size : new google.maps.Size(71, 71),
                origin : new google.maps.Point(0, 0),
                anchor : new google.maps.Point(17, 34),
                scaledSize : new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map : map,
                icon : icon,
                title : place.name,
                position : place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });

        map.fitBounds(bounds);
    });
}
google.maps.event.addDomListener(window, 'load', initMap);

function placeMarkerAndPanTo(latLng, map) {
    if (myMarker != null) {
        myMarker.setMap(null);
    }
    myMarker = new google.maps.Marker({
        position : latLng,
        map : map
    });
    map.panTo(latLng);
}

$('#village').on('change', function() {
    var name = $("#village option:selected").text();
    name = name + ", " + districtAddress;
    loadGeocoder(name, 14);
    return true;
});

$('#district').on('change', function() {

    var name = $("#select-commune option:selected").text();
    districtAddress = name + ", " + initAddress;
    loadGeocoder(districtAddress, 12);

    return true;
});

function loadGeocoder(myAdress, zoom, point) {
    if (geocoder) {
        geocoder.geocode({
            'address' : myAdress
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                    map.setCenter(results[0].geometry.location);
                    map.setZoom(zoom);
                    $('#pac-input').val(myAdress);
                }
            }
        });

        if(point != null) {
            placeMarkerAndPanTo(point, map);
        }
    }
}